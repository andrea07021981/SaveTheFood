# SaveTheFood (Under development)
App for keeping trace of food in the fridge and for searching recipes based either on general filters or on saved foods.
I've build one activity app with some navigation levels. I worked with Jetpack components like Room, Databinding, Livedata, Nagivation, Viewmodel, Workmanager. The pattern used is the MVVM.
IMPORTANT: Create a free api key here and change it in file ApiKey ----> https://spoonacular.com/food-api



<img align="left" width="280" height="500" src="images/login.png">
<img align="left" width="280" height="500" src="images/signUp.png">
<img align="left" width="280" height="500" src="images/home.png">
<img align="left" width="280" height="500" src="images/fooddetail.png">

<img align="left" width="280" height="500" src="images/nav.png">

<img align="left" width="280" height="500" src="images/search.png">

<img align="left" width="280" height="500" src="images/found.png">

<img align="left" width="280" height="500" src="images/recipes.png">

<img align="left" width="280" height="500" src="images/recipedetail.png">

<img align="left" width="280" height="500" src="images/cook.png">

