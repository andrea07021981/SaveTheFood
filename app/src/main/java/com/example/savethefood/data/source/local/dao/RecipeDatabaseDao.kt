package com.example.savethefood.data.source.local.dao

import androidx.room.Dao


/**
 * Defines methods for using the entities class with Room.
 */
@Dao
interface RecipeDatabaseDao