package com.example.savethefood.util

import android.app.Activity
import android.app.SearchManager
import android.content.Context
import android.util.Patterns
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.RecyclerView
import com.example.savethefood.data.Result
import com.example.savethefood.data.domain.FoodDomain
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

/**
 * Extension function to create a SearchView Item and manage the text inserted
 * @param   activity    Current context
 * @param   hint        Hint for the edit text
 * @param   block       High Order function to manage the text change
 */
// TODO add onchange like change password
fun MenuItem.configSearchView(activity: Activity, hint: String = "Type Value", block: (String) -> Unit) {
    val searchView = actionView as SearchView
    val searchManager = activity.getSystemService(Context.SEARCH_SERVICE) as SearchManager

    with(searchView) {
        setSearchableInfo(searchManager.getSearchableInfo(activity.componentName))

        //Manage search view
        //making the searchview consume all the toolbar when open
        let { it ->
            it.maxWidth= Int.MAX_VALUE
            it.queryHint = hint
            setOnQueryTextListener(object : SearchView.OnQueryTextListener {

                override fun onQueryTextChange(newText: String): Boolean {
                    //action while typing
                    block(newText)
                    return false
                }

                override fun onQueryTextSubmit(query: String): Boolean {
                    //action when type Enter
                    return false
                }

            })
        }
    }
}

// TODO replace result with ApiCallStatus?
inline fun <T> ViewModel.launchDataLoad(loader: MutableLiveData<Result<T>>, crossinline block: suspend () -> Result<T>): Job {
    return viewModelScope.launch {
        try {
            loader.value = Result.Loading
            loader.value = block()
        } catch (error: Exception) {
            loader.value = Result.ExError(error)
        } finally {
            loader.value = Result.Idle
        }
    }
}

fun Double?.isValidDouble(): Boolean {
    return this != null && this != 0.0
}

fun String.isValidEmail(): Boolean = Patterns.EMAIL_ADDRESS.matcher(this).matches()

fun String.isValidPassword(): Boolean = this.length in 8..16